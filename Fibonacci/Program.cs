﻿using System;

namespace Fibonacci {
    class Program {
        static void Main(string[] args) {
            Console.WriteLine("Hello Fibonacci, spaggeti bologenesi");

            string input;
            int length;

            int a = 1;
            int b = 2;
            int placeholder;
            int sum = 2;
            string output = a + " " + b + " ";

            Console.WriteLine("Please enter length of Fibonacci sequence:");
            input = Console.ReadLine();
            length = Convert.ToInt32(input);

            while(b <= 4000000)
            {
                output += a + b + " ";
                placeholder = b;
                b = a + b;
                a = placeholder;
                if(b % 2 == 0)
                {
                    sum += b;
                }
            }
            Console.WriteLine(output);
            Console.WriteLine("Sum of even numbers:");
            Console.WriteLine(sum);
        }
    }
}
